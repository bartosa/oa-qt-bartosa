#include <iostream>
#include <cstdlib>
#include <QDirIterator>
#include <QFileInfo>
#include <QtCore/QDateTime>

using namespace std;

bool isValid(char* number){
    char * pEnd;

    if(strcmp(number, "0") == 0){return true;} // 0-t is kérhet, bár nem sok értelme van; de én ezt azért nem szórnám ki invaliddal.

    long int num = strtol (number,&pEnd,10);
    if(num==0L){ return false;}
    else{
        if(num<0) return false;
        else return true;
    }
}

int main(int argc, char ** argv)
{
    cout << "-- Aniko Bartos Qt Homework -- " << endl << endl ;

    if(argc > 3){
        cout<<"iAy, carramba! Too much parameter..."<<endl;
        return 1;
    }
    else if(argc < 3){
        cout<<"iAy, carramba! Too less parameter..."<<endl;
        return 1;
    }
    else if(!isValid(argv[2])){
        cout<<"Invalid number given\n";
        return 1;
    }
    else{
        QDirIterator dirIterator(argv[1],
                                QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot,
                                QDirIterator::Subdirectories);

        int counter=0;
        while(dirIterator.hasNext() && counter<atoi(argv[2])){
            QString path = dirIterator.next();
            QFileInfo info = dirIterator.fileInfo();

            if(info.isDir()){
                cout<<"Directory: "<< path.toStdString() << endl;
            }
            else if(info.isFile()){
                QString fileprop("\t%1 kb   %2  %3");
                cout<< fileprop.arg(QString::number(info.size()/1024).rightJustified(10)) // left-el furán nézett ki..
                                .arg(info.lastModified().toString("yyyy-MM-dd hh:mm").leftJustified(30,' '))
                                .arg(dirIterator.fileName())
                       .toStdString() << std::endl;
            }
            else{
                cout<<"NOT DIR NOT FILE! What's that?! ( " << path.toStdString() << ")" <<endl;
            }
            counter++;
        }
    }

    return 0;
}
